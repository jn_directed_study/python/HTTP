import urllib
import httplib2
import json

myURL = 'http://localhost'
myPage = '/API/User/6'
myHTTPPort = 8181
myHeader = {'Content-type': 'application/json', 'Authorization': 'mENFYK29QAHBPxTpqr759XVA4OuvyCiFdw9jU9Sl5OD8Q26eWk'}

#
# GET Method
#
h = httplib2.Http()
(resp, content) = h.request(myURL + ':' + str(myHTTPPort) + myPage,
                            'GET',
                            headers=myHeader
                            )
print('GET Method')
print('Variavel RESP')
print('- ' * 20)
print(json.dumps(resp, indent=2))
print('- ' * 20)
print('- ' * 20)
print('Variavel CONTENT')
print('- ' * 20)
print(json.dumps(json.loads(content), indent=2))
print('- ' * 20)

#
# POST Method /API/Login
#
h = httplib2.Http()
myURL = 'http://localhost'
myPage = '/API/Login'
myHTTPPort = 8181
myBody = {'username': 'radiusServer1', 'password': 'myRadiusServer123'}
myHeader = {'Content-type': 'application/json; charset=UTF-8'}

h = httplib2.Http()
(resp, content) = h.request(myURL + ':' + str(myHTTPPort) + myPage,
                            'POST',
                            body=json.dumps(myBody),
                            headers=myHeader
                            )
print('POST Method')
print('Variavel RESP')
print('- ' * 20)
print(json.dumps(resp, indent=2))
print('- ' * 20)
print('- ' * 20)
print('Variavel CONTENT')
print('- ' * 20)
content = json.loads(content)
print(content['codeReturn']['myToken'])
print(json.dumps(content, indent=2))
print('- ' * 20)